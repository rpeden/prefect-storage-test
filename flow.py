import os

from prefect import task, flow, get_run_logger, get_client
from prefect.infrastructure import Process
import platform
import sys
import asyncio


@task(name="I'm running in ACI!")
async def aci_task(n):
    logger = get_run_logger()
    logger.info(f"Hello from the ACI task run #{n}!")


@flow(name="ACI test flow")
async def hi():
    logger = get_run_logger()
    logger.info("Hi from the ACI flow!")

    for x in range(0, 5):
        await aci_task(x)
        await asyncio.sleep(1)

    # logger.info(json.dumps(env, indent=4))


@task
def log_platform_info():
    logger = get_run_logger()
    platform.system()
    # a nifty formatter to display our output in orderly columns
    table_text = "{0:>22}:  {1:<50}".format
    # All ACI container hostnames should start with 'SandboxHost'
    logger.info(table_text("Host's network name", f"{platform.node()} 🚀"))
    logger.info(table_text("Python version", platform.python_version()))
    logger.info(table_text("Platform information (instance type)", platform.platform()))
    logger.info(table_text("OS/Arch", f"{sys.platform}/{platform.machine()}"))

    for k, v in os.environ.items():
        # we expect to see several CLOUD_RUN* environment variables
        if k.startswith("CLOUD_RUN"):
            logger.info(table_text(k, v))


@flow()
def healthcheck():
    log_platform_info()
