from flow import healthcheck
from prefect import get_client
from prefect.deployments import Deployment
from prefect_gitlab import GitLab
from prefect_gcp.cloud_run import CloudRunJob

client = get_client()

gcs_block = GitLab.load("my-gitlab")
cloud_run_job_block = CloudRunJob.load("my-gcp-infrastructure")


deployment = Deployment.build_from_flow(
    flow=healthcheck,
    name="GCP test flow",
    storage=gcs_block,
    infrastructure=cloud_run_job_block,
)

if __name__ == "__main__":
    deployment.apply()
